﻿using System.Net.Http;
using RabbitMQ.Client;
using SMETS2.Req.ClassLibrary.Models;
using SMETS2.Req.ClassLibrary.Repository;

namespace SMETS2.Req.ClassLibrary.Contracts
{
    public interface IRequestService
    {
        void DccConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, IRequestTrackingRepository requestTrackingRepository, int apiDelayMs, string workerName);
        void NotConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, IRequestTrackingRepository requestTrackingRepository, string tmaApiServer, int apiDelayMs, string workerName);
        void DefaultNotConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, string tmaApiServer, int apiDelayMs, string workerName);
        void ScheduleReadReadyConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, string tmaApiServer, int apiDelayMs, string workerName);
        void AddMessageToQueue(string exchangeName, string queueName, string message, int recordCount, IModel channelPublish);
        void NotAlertConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, string tmaApiServer, int apiDelayMs, string workerName);
    }
}
