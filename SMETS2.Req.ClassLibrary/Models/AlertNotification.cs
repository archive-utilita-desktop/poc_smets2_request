﻿using System;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace SMETS2.Req.ClassLibrary.Models
{
    public class AlertNotification
    {
        [JsonPropertyName("id")]
        public decimal? Id { get; set; }
        [JsonPropertyName("alertTimestamp")]
        public DateTime? AlertTimeStamp { get; set; }
        [JsonPropertyName("alertType")]
        public string AlertType { get; set; }
        [JsonPropertyName("alertSubType")]
        public string AlertSubType { get; set; }
        [JsonPropertyName("alertCode")]
        public string AlertCode { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("alertBody")]
        public Dictionary<string,string> AlertBody { get; set; }
        [JsonPropertyName("deviceId")]
        public string DeviceId { get; set; }
        public string RequestReference { get; set; }
    }
}