﻿namespace SMETS2.Req.ClassLibrary.Models
{
    public class ErrorModel
    {
        public string ServiceName { get; set; }
        public string MessagePayload { get; set; }
        public string ErrorDescription { get; set; }
        public Request Route { get; set; }
    }
}
