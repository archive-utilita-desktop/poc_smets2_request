﻿namespace SMETS2.Req.ClassLibrary.Models
{
    public class Request
    {
        public string Id { get; set; }
        public string RouteName { get; set; }
        public string SourceExchange { get; set; }
        public string SourceQueue { get; set; }
        public string Tenant { get; set; }
        public string HttpVerb { get; set; }
        public string UrlPath { get; set; }
        public string TargetExchange { get; set; }
        public string TargetQueue { get; set; }
    }
}
