﻿using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SMETS2.Req.ClassLibrary.Models
{
    public class RequestTracking
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("RequestGuid")]
        [Required]
        public string RequestGuid { get; set; }

        [BsonElement("RequestReference")]
        [Required]
        public string RequestReference { get; set; }

        [BsonElement("RequestReferenceDate")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [Required]
        public DateTime RequestReferenceDate { get; set; }

        [BsonElement("RequestUrl")]
        [Required]
        public string RequestUrl { get; set; }
    }
}
