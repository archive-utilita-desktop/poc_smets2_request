﻿using System;
using System.Text.Json.Serialization;

namespace SMETS2.Req.ClassLibrary.Models
{
    public class ScheduleReadReady
    {
        [JsonPropertyName("id")]
        public decimal? Id { get; set; }
        [JsonPropertyName("deviceId")]
        public string DeviceId { get; set; }
        [JsonPropertyName("readingDate")]
        public DateTime? ReadingDate { get; set; }
        [JsonPropertyName("mPxN")]
        public string MPxN { get; set; }
        [JsonPropertyName("meterReadingType")]
        public string MeterReadingType { get; set; }
        [JsonPropertyName("meterReadingTypeDescription")]
        public string MeterReadingTypeDescription { get; set; }
        [JsonPropertyName("readingType")]
        public string ReadingType { get; set; }
        [JsonPropertyName("readingTypeDescription")]
        public string ReadingTypeDescription { get; set; }
        [JsonPropertyName("readingResult")]
        public string ReadingResult { get; set; }
    }
}
