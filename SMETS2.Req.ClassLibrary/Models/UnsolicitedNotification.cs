﻿using System;

namespace SMETS2.Req.ClassLibrary.Models
{
    public class UnsolicitedNotification
    {
        public string Tenant { get; set; }
        public string RequestReference { get; set; }
        public DateTime? NotificationDate { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Refs { get; set; }
        public string Payload { get; set; }
    }
}
