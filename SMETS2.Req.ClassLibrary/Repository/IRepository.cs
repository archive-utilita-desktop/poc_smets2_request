﻿using System.Collections.Generic;

namespace SMETS2.Req.ClassLibrary.Repository
{
    public interface IRepository<T> where T : class
    {
        public List<T> GetAll();
        public T Get(string id);
        public T Create(T t);
        public void Update(string id, T t);
        public void Delete(string id);
    }
}
