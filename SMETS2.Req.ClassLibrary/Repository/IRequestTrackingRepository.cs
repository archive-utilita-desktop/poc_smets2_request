﻿using SMETS2.Req.ClassLibrary.Models;

namespace SMETS2.Req.ClassLibrary.Repository
{
    public interface IRequestTrackingRepository : IRepository<RequestTracking>
    {
        public RequestTracking GetByGuid(string requestGuid);
        public RequestTracking GetByReference(string requestReference);
        public void DeleteByGuid(string requestGuid);
        public void DeleteByReference(string requestReference);
    }
}
