﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SMETS2.Req.ClassLibrary.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        private readonly IMongoCollection<T> tMongoCollection;

        public Repository(IMongoDatabase database)
        {
            tMongoCollection = database.GetCollection<T>(typeof(T).Name);
        }

        public List<T> GetAll()
        {
            return tMongoCollection.Find(T => true).ToList();
        }

        public T Get(string id)
        {
            var objectId = new ObjectId(id);

            FilterDefinition<T> filter = Builders<T>.Filter.Eq("_id", objectId);

            return tMongoCollection.Find(filter).FirstOrDefault();
        }

        public T Create(T t)
        {
            tMongoCollection.InsertOne(t);
            return t;
        }

        public void Update(string id, T t)
        {
            var objectId = new ObjectId(id);

            FilterDefinition<T> filter = Builders<T>.Filter.Eq("_id", objectId);

            tMongoCollection.ReplaceOne(filter, t);
        }

        public void Delete(string id)
        {
            var objectId = new ObjectId(id);

            FilterDefinition<T> filter = Builders<T>.Filter.Eq("_id", objectId);

            tMongoCollection.DeleteOne(filter);
        }
    }
}
