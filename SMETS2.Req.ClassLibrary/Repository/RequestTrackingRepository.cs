﻿using MongoDB.Driver;
using SMETS2.Req.ClassLibrary.Models;

namespace SMETS2.Req.ClassLibrary.Repository
{
    public class RequestTrackingRepository : Repository<RequestTracking>, IRequestTrackingRepository
    {
        private readonly IMongoCollection<RequestTracking> tMongoCollection;

        public RequestTrackingRepository(IMongoDatabase database) : base(database)
        {
            tMongoCollection = database.GetCollection<RequestTracking>(typeof(RequestTracking).Name);
        }

        public RequestTracking GetByGuid(string requestGuid)
        {
            FilterDefinition<RequestTracking> filter = Builders<RequestTracking>.Filter.Eq(x => x.RequestGuid, requestGuid);

            return tMongoCollection.Find(filter).FirstOrDefault();
        }

        public RequestTracking GetByReference(string requestReference)
        {
            FilterDefinition<RequestTracking> filter = Builders<RequestTracking>.Filter.Eq(x => x.RequestReference, requestReference);

            return tMongoCollection.Find(filter).FirstOrDefault();
        }

        public void DeleteByGuid(string requestGuid)
        {
            FilterDefinition<RequestTracking> filter = Builders<RequestTracking>.Filter.Eq(x => x.RequestGuid, requestGuid);

            tMongoCollection.DeleteOne(filter);
        }

        public void DeleteByReference(string requestReference)
        {
            FilterDefinition<RequestTracking> filter = Builders<RequestTracking>.Filter.Eq(x => x.RequestReference, requestReference);

            tMongoCollection.DeleteOne(filter);
        }

    }
}
