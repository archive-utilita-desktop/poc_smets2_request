﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SMETS2.Req.ClassLibrary.Contracts;
using SMETS2.Req.ClassLibrary.Models;
using SMETS2.Req.ClassLibrary.Repository;
using RabbitMQ.Client.Exceptions;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SMETS2.Req.ClassLibrary.Services
{
    public class RequestService : IRequestService
    {
        private readonly ILogger<RequestService> logger;

        private Stopwatch stopwatch;

        public RequestService(ILogger<RequestService> logger)
        {
            this.logger = logger;
            stopwatch = new Stopwatch();
        }

        public void DccConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, IRequestTrackingRepository requestTrackingRepository, int apiDelayMs, string workerName)
        {
            var totalMessageCount = 0;
            string message = string.Empty;
 
            var consumer = new EventingBasicConsumer(channelConsume);
            consumer.Received += async (model, ea) =>
            {
                logger.LogDebug("Begin DccConsumeMessage using Route: {0}", requestRoute.RouteName);
                
                stopwatch.Restart();

                try
                {
                    // add delay prior to calling API to ensure number of requests/second can be handled by TMA  
                    Thread.Sleep(apiDelayMs);

                    totalMessageCount++;

                    message = Encoding.UTF8.GetString(ea.Body.ToArray());

                    logger.LogDebug("DccConsumeMessage reformatting message: {0}", message);

                    // -----------------------------------------------------------
                    // reformat message prior to calling API 
                    // -----------------------------------------------------------

                    // replace any 'null' values with "" 
                    string newMessage = message.Replace("null", "\"\"");
 
                    // convert the json message into json object for manipulation 
                    JObject jsonObjMessage = JObject.Parse(newMessage);

                    // identify the RequestGuid from the json object 
                    string requestGuid = (string)jsonObjMessage["RequestGuid"];

                    // remove the RequestGuid from the json object 
                    jsonObjMessage.Property("RequestGuid").Remove();

                    // convert json object to http string content;
                    // n.b. ToString() method is required to remove extra set of curly brackets added during json object conversion  
                    StringContent requestData = new StringContent(content: jsonObjMessage.ToString(), encoding: Encoding.UTF8,
                        mediaType: "application/json");

                    logger.LogDebug("DccConsumeMessage calling API with Request Guid: {0}", requestGuid);

                    // -----------------------------------------------------------
                    // call the API
                    // -----------------------------------------------------------

                    // construct URL to be called 
                    string requestUri = sbsApiServer + requestRoute.UrlPath;

                    logger.LogInformation("DccConsumeMessage calling API using Uri {0}", requestUri);

                    // call the API
                    HttpResponseMessage response = await httpClient.PostAsync(requestUri: requestUri, content: requestData);
                    var responseData = await GetResponseContent(response);

                    if (!response.IsSuccessStatusCode)
                        throw new Exception($"Unable to fetch data from TMA's API for Request Guid: {requestGuid}. " +
                            $"Response Status: {response.StatusCode}. Response Content: {responseData}");

                    logger.LogDebug("DccConsumeMessage API call successful. Deserialising message: {0}", responseData);

                    // -----------------------------------------------------------
                    // reformat response data for MongoDB  
                    // -----------------------------------------------------------

                    // format the data (to object) to remove excessive backslashes, e.g. "\"{\\\"requestReference\\\":\\\"5a1a61270f3c43d6baa47ee6a032e33e\\\"}\""
                    object formattedResponseData = JsonConvert.DeserializeObject(responseData);

                    // convert the formatted json response message into json object for manipulation 
                    JObject jsonObjResponseData = JObject.Parse((string)formattedResponseData);

                    // identify the requestReference from the json object 
                    string requestReference = (string)jsonObjResponseData["requestReference"];

                    // build the RequestTracking MongoDb record  
                    RequestTracking requestTracking = new RequestTracking
                    {
                        RequestGuid = requestGuid,
                        RequestReference = requestReference,
                        RequestReferenceDate = DateTime.Now,
                        RequestUrl = requestUri
                    };

                    logger.LogDebug("DccConsumeMessage created RequestTracking object with properties: {0}, {1}, {2}, {3}", 
                        requestTracking.RequestGuid,
                        requestTracking.RequestReference,
                        requestTracking.RequestReferenceDate,
                        requestTracking.RequestUrl);

                    // -----------------------------------------------------------
                    // add record to MongoDB  
                    // -----------------------------------------------------------
                    requestTrackingRepository.Create(requestTracking);

                    logger.LogDebug("DccConsumeMessage logged Request ({0}) to MongoDB", requestTracking.RequestGuid);

                    // -----------------------------------------------------------
                    // Create json message for inbound Staging DCC queue    
                    // -----------------------------------------------------------

                    //Create a new json object to return message back to DCC
                    JObject requestTrackingJson = new JObject();
                    requestTrackingJson.Add("RequestGuid", requestTracking.RequestGuid);
                    requestTrackingJson.Add("RequestReference", requestTracking.RequestReference);
                    requestTrackingJson.Add("RequestReferenceDate", requestTracking.RequestReferenceDate);
                    requestTrackingJson.Add("RequestUrl", requestTracking.RequestUrl);

                    // convert the json object to json string for adding to return queue
                    var requestTrackingMessage = requestTrackingJson.ToString(Formatting.None);

                    // -----------------------------------------------------------
                    // Add return message to RabbitMQ queue 
                    // -----------------------------------------------------------
                    AddMessageToQueue(exchangeName: requestRoute.TargetExchange,
                        queueName: requestRoute.TargetQueue,
                        message: requestTrackingMessage,
                        recordCount: totalMessageCount,
                        channelPublish: channelPublish);

                    // acknowledge that we have consumed message successfully 
                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                    logger.LogInformation("Processed record in {0}ms. Total message count: {1}", stopwatch.ElapsedMilliseconds, totalMessageCount);
                }
                catch (BrokerUnreachableException ex)
                {
                    // TO DO: Add Retry logic
                    logger.LogError("Unable to communicate with RabbitMQ with Exception: {0} and Inner Exception: {1}", ex.Message, ex.InnerException.Message);
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogError("Error processing message. Error Message: {0}", ex.Message);

                    var error = new ErrorModel
                    {
                        ServiceName = workerName,
                        MessagePayload = message,
                        ErrorDescription = ex.Message,
                        Route = requestRoute
                    };

                    PublishErrorMessage(channelPublish, error);
                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                }
                finally
                {
                    stopwatch.Reset();
                }
            };
            channelConsume.BasicConsume(queue: requestRoute.SourceQueue, autoAck: false, consumer: consumer);

            Console.ReadLine();
        }


        public void NotConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, IRequestTrackingRepository requestTrackingRepository, string tmaApiServer, int apiDelayMs, string workerName)
        {
            var totalMessageCount = 0;
            string message = string.Empty;

            var consumer = new EventingBasicConsumer(channelConsume);
            consumer.Received += async (model, ea) =>
            {
                logger.LogDebug("Begin NotConsumeMessage using Route: {0}", requestRoute.RouteName);
                
                stopwatch.Restart();

                try
                {
                    // add delay prior to calling API to ensure number of requests/second can be handled by TMA  
                    Thread.Sleep(apiDelayMs);

                    totalMessageCount++;

                    message = Encoding.UTF8.GetString(ea.Body.ToArray());

                    // -----------------------------------------------------------
                    // reformat message prior to calling API 
                    // -----------------------------------------------------------

                    // convert the json message into json object for manipulation 
                    JObject jsonObjMessage = JObject.Parse(message);

                    // identify the RequestReference from the json object; n.b. it contains hyphens in the guid 
                    string requestReferenceHyphens = (string)jsonObjMessage["RequestReference"];

                    // remove hyphens from RequestReference
                    string requestReference = requestReferenceHyphens.Replace("-", "");

                    // identify the URL from the json object 
                    string requestUrlTma = (string)jsonObjMessage["Refs"][0];

                    logger.LogDebug("NotConsumeMessage extracted the Request URL: {0}", requestUrlTma);

                    Match requestUrlTmaSuffix = Regex.Match(requestUrlTma, @"(?<=[\.uk])\/api.*$");

                    if (!requestUrlTmaSuffix.Success)
                        throw new Exception($"Could not parse the Request URL: {requestUrlTma} for Request Reference: {requestReferenceHyphens}");

                    // replace the TMA base address with SBS base address 
                    //string requestUri = requestUrlTma.Replace(tmaApiServer, sbsApiServer);
                    string requestUri = sbsApiServer + requestUrlTmaSuffix.Value;

                    // -----------------------------------------------------------
                    // call the API 
                    // -----------------------------------------------------------

                    logger.LogInformation("NotConsumeMessage calling API using Uri {0}", requestUri);

                    // call the API
                    HttpResponseMessage response = await httpClient.GetAsync(requestUri: requestUri);
                    var responseData = await GetResponseContent(response);

                    if (!response.IsSuccessStatusCode)
                        throw new Exception($"Unable to fetch data from TMA's API for Request Reference: {requestReferenceHyphens}. " +
                            $"Response Status: {response.StatusCode}. Response Content: {responseData}");

                    logger.LogDebug("NotConsumeMessage API call successful. Deserialising message: {0}", responseData);

                    // -----------------------------------------------------------
                    // reformat response data   
                    // -----------------------------------------------------------

                    // format the data (to object) to remove excessive backslashes, e.g. "\"{\\\"requestReference\\\":\\\"5a1a61270f3c43d6baa47ee6a032e33e\\\"}\""
                    object formattedResponseDataObject = JsonConvert.DeserializeObject(responseData);

                    // format the data to a string (from an object type)  
                    string formattedResponseDataString = (string)formattedResponseDataObject;

                    // -----------------------------------------------------------
                    // read record from MongoDB using RequestReference (to get RequestGuid)
                    // -----------------------------------------------------------
                    RequestTracking requestTracking = requestTrackingRepository.GetByReference(requestReference);

                    string queueName = "";
                    string jsonQueueReturnData = "";

                    // ---------------------------------------------------------------------------
                    // cater for unsolicited messages (i.e do NOT have a RequestGuid) in MongoDb   
                    // ---------------------------------------------------------------------------
                    if (requestTracking is null)
                    {
                        queueName = "not." + requestRoute.Tenant.ToLower().Substring(0, 3) + ".in.default.q"; // "sup" or "ins"
                        jsonQueueReturnData = message;
                        logger.LogInformation("Unsolicited message with no RequestGuid published to Default queue: {0}", queueName);
                        logger.LogDebug("** handled unsolicited request: {0}", message);
                    }
                    else
                    {
                        // -----------------------------------------------------------
                        // add RequestGuid (by prefixing it) to formatted json response data  
                        // n.b. depends on whether the data is a simple json object or json array
                        // -----------------------------------------------------------

                        queueName = requestRoute.TargetQueue; 

                        string jsonQueueReturnDataAddRequestGuid = String.Empty;

                        if (formattedResponseDataString.Substring(startIndex: 0, length: 1) == "{")
                        {
                            jsonQueueReturnDataAddRequestGuid = Regex.Replace(input: formattedResponseDataString, pattern: "^{",
                                replacement: "{\"requestGuid\":\"" + (string)requestTracking.RequestGuid + "\",");
                        }
                        else
                        {
                            jsonQueueReturnDataAddRequestGuid = Regex.Replace(input: formattedResponseDataString, pattern: "^\\[",
                                replacement: "{\"requestGuid\":\"" + (string)requestTracking.RequestGuid + "\",\"responseData\": \\[");

                            jsonQueueReturnDataAddRequestGuid = Regex.Replace(input: jsonQueueReturnDataAddRequestGuid, pattern: "\\]$",
                                replacement: "\\]}");

                            jsonQueueReturnDataAddRequestGuid = jsonQueueReturnDataAddRequestGuid.Replace("\\", "");
                            
                        }

                        // finally, replace any 'null' values with "" 
                        jsonQueueReturnData = jsonQueueReturnDataAddRequestGuid.Replace("null", "\"\"");

                    }

                    // -----------------------------------------------------------
                    // Add return message to RabbitMQ queue 
                    // -----------------------------------------------------------
                    AddMessageToQueue(exchangeName: requestRoute.TargetExchange,
                        queueName: queueName,
                        message: jsonQueueReturnData,
                        recordCount: totalMessageCount,
                        channelPublish: channelPublish);

                    // acknowledge that we have consumed message successfully 
                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                    logger.LogInformation("Processed record in {0}ms. Total message count: {1}", stopwatch.ElapsedMilliseconds, totalMessageCount);
                }
                catch (BrokerUnreachableException ex)
                {
                    // TO DO: Add Retry logic
                    logger.LogError("Unable to communicate with RabbitMQ with Exception: {0} and Inner Exception: {1}", ex.Message, ex.InnerException.Message);
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogError("Error processing message. Error Message: {0}", ex.Message);

                    var error = new ErrorModel
                    {
                        ServiceName = workerName,
                        MessagePayload = message,
                        ErrorDescription = ex.Message,
                        Route = requestRoute
                    };

                    PublishErrorMessage(channelPublish, error);

                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                }
                finally
                {
                    stopwatch.Reset();
                }
            };
            channelConsume.BasicConsume(queue: requestRoute.SourceQueue, autoAck: false, consumer: consumer);

            Console.ReadLine();
        }

        public void DefaultNotConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, string tmaApiServer, int apiDelayMs, string workerName)
        {
            var totalMessageCount = 0;
            string message = string.Empty;

            var consumer = new EventingBasicConsumer(channelConsume);
            consumer.Received += async (model, ea) =>
            {
                logger.LogDebug("Begin DefaultNotConsumeMessage using Route: {0}", requestRoute.RouteName);

                stopwatch.Restart();

                try
                {
                    // add delay prior to calling API to ensure number of requests/second can be handled by TMA  
                    Thread.Sleep(apiDelayMs);

                    totalMessageCount++;

                    message = Encoding.UTF8.GetString(ea.Body.ToArray());

                    // -----------------------------------------------------------
                    // reformat message prior to calling API 
                    // -----------------------------------------------------------

                    // convert the json message into json object for manipulation 
                    JObject jsonObjMessage = JObject.Parse(message);

                    // identify the RequestReference from the json object; n.b. it contains hyphens in the guid 
                    string requestReferenceHyphens = (string)jsonObjMessage["RequestReference"];

                    // identify the URL from the json object 
                    string requestUrlTma = (string)jsonObjMessage["Refs"][0];

                    Match requestUrlTmaSuffix = Regex.Match(requestUrlTma, @"(?<=[\.uk])\/api.*$");

                    if (!requestUrlTmaSuffix.Success)
                        throw new Exception($"Could not parse the Request URL: {requestUrlTma} for Request Reference: {requestReferenceHyphens}");

                    // replace the TMA base address with SBS base address 
                    string requestUri = sbsApiServer + requestUrlTmaSuffix.Value;

                    // -----------------------------------------------------------
                    // setup the output
                    // -----------------------------------------------------------

                    // create the output and set initial values
                    UnsolicitedNotification queueOutput = new UnsolicitedNotification
                    {
                        Tenant = requestRoute.Tenant,
                        RequestReference = requestReferenceHyphens.Replace("-", ""),
                        NotificationDate = DateTime.Now,
                        Type = (string)jsonObjMessage["Type"],
                        SubType = (string)jsonObjMessage["SubType"],
                        Refs = requestUrlTma
                    };

                    // -----------------------------------------------------------
                    // call the API 
                    // -----------------------------------------------------------

                    logger.LogInformation("DefaultNotConsumeMessage calling API using Uri {0}", requestUri);

                    // call the API
                    HttpResponseMessage response = await httpClient.GetAsync(requestUri: requestUri);
                    var responseData = await GetResponseContent(response);
                   
                    if (!response.IsSuccessStatusCode)
                        throw new Exception($"Unable to fetch data from TMA's API for Request Reference: {requestReferenceHyphens}. " +
                            $"Response Status: {response.StatusCode}. Response Content: {responseData}");
                    
                    logger.LogDebug("DefaultNotConsumeMessage API call successful. Deserialising message: {0}", responseData);

                    // -----------------------------------------------------------
                    // reformat response data   
                    // -----------------------------------------------------------

                    // strip extra escape backslashes out of the string
                    responseData = responseData.Replace("\\", "");
                    queueOutput.Payload = responseData.Replace("null", "\"\"");

                    // serialize the queueOutput into a string to add to the queue
                    string jsonQueueReturnData = JsonConvert.SerializeObject(queueOutput);

                    // -----------------------------------------------------------
                    // Add return message to RabbitMQ queue 
                    // -----------------------------------------------------------
                    AddMessageToQueue(exchangeName: requestRoute.TargetExchange,
                        queueName: requestRoute.TargetQueue,
                        message: jsonQueueReturnData,
                        recordCount: totalMessageCount,
                        channelPublish: channelPublish);

                    // acknowledge that we have consumed message successfully 
                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                    logger.LogInformation("Processed record in {0}ms. Total message count: {1}", stopwatch.ElapsedMilliseconds, totalMessageCount);
                }
                catch (BrokerUnreachableException ex)
                {
                    // TO DO: Add Retry logic
                    logger.LogError("Unable to communicate with RabbitMQ with Exception: {0} and Inner Exception: {1}", ex.Message, ex.InnerException.Message);
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogError("Error processing message. Error Message: {0}", ex.Message);

                    var error = new ErrorModel
                    {
                        ServiceName = workerName,
                        MessagePayload = message,
                        ErrorDescription = ex.Message,
                        Route = requestRoute
                    };

                    PublishErrorMessage(channelPublish, error);

                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                }
                finally
                {
                    stopwatch.Reset();
                }
            };

            channelConsume.BasicConsume(queue: requestRoute.SourceQueue, autoAck: false, consumer: consumer);

            Console.ReadLine();
        }

        public void ScheduleReadReadyConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, string tmaApiServer, int apiDelayMs, string workerName)
        {
            var totalMessageCount = 0;
            string message = string.Empty;

            var consumer = new EventingBasicConsumer(channelConsume);
            consumer.Received += async (model, ea) =>
            {
                logger.LogDebug("Begin ScheduleReadReadyConsumeMessage using Route: {0}", requestRoute.RouteName);

                stopwatch.Restart();

                try
                {
                    // add delay prior to calling API to ensure number of requests/second can be handled by TMA  
                    Thread.Sleep(apiDelayMs);

                    totalMessageCount++;

                    message = Encoding.UTF8.GetString(ea.Body.ToArray());

                    // -----------------------------------------------------------
                    // reformat message prior to calling API 
                    // -----------------------------------------------------------

                    // convert the json message into json object for manipulation 
                    JObject jsonObjMessage = JObject.Parse(message);

                    // identify the RequestReference from the json object; n.b. it contains hyphens in the guid 
                    string requestReferenceHyphens = (string)jsonObjMessage["RequestReference"];

                    // remove hyphens from RequestReference
                    string requestReference = requestReferenceHyphens.Replace("-", "");

                    // identify the URL from the json object 
                    string requestUrlTma = (string)jsonObjMessage["Refs"][0];

                    logger.LogDebug("ScheduleReadReadyConsumeMessage extracted the Request URL: {0}", requestUrlTma);

                    Match requestUrlTmaSuffix = Regex.Match(requestUrlTma, @"(?<=[\.uk])\/api.*$");

                    if (!requestUrlTmaSuffix.Success)
                        throw new Exception($"Could not parse the Request URL: {requestUrlTma} for Request Reference: {requestReferenceHyphens}");

                    // replace the TMA base address with SBS base address 
                    string requestUri = sbsApiServer + requestUrlTmaSuffix.Value;

                    // -----------------------------------------------------------
                    // call the API 
                    // -----------------------------------------------------------

                    logger.LogInformation("ScheduleReadReadyConsumeMessage calling API using Uri {0}", requestUri);

                    // call the API
                    HttpResponseMessage response = await httpClient.GetAsync(requestUri: requestUri);
                    var responseData = await GetResponseContent(response);

                    if (!response.IsSuccessStatusCode)
                        throw new Exception($"Unable to fetch data from TMA's API for Request Reference: {requestReferenceHyphens}. " +
                            $"Response Status: {response.StatusCode}. Response Content: {responseData}");

                    logger.LogDebug("ScheduleReadReadyConsumeMessage API call successful. Deserialising message: {0}", responseData);

                    // -----------------------------------------------------------
                    // reformat response data   
                    // -----------------------------------------------------------

                    // format the data (to object) to remove excessive backslashes, e.g. "\"{\\\"requestReference\\\":\\\"5a1a61270f3c43d6baa47ee6a032e33e\\\"}\""
                    object formattedResponseDataObject = JsonConvert.DeserializeObject(responseData);

                    // deserialize the response into a ScheduleReadReady object
                    // using system.text.json to ensure deciaml values retain their precision
                    ScheduleReadReady readingResponse = System.Text.Json.JsonSerializer.Deserialize<ScheduleReadReady>(formattedResponseDataObject.ToString()) ;

                    // construct the new TargetQueue using the ReadingType
                    requestRoute.TargetQueue = $"dcc.sup.in.{readingResponse.ReadingType.ToLower().Replace(" ", "").Replace("-", "")}.q";

                    // serialize the ScheduleReadReadyback into JSON 
                    string formattedResponseDataString = System.Text.Json.JsonSerializer.Serialize(readingResponse);

                    // finally, replace any 'null' values with "" 
                    var jsonQueueReturnData = formattedResponseDataString.Replace("null", "\"\"");

                    // -----------------------------------------------------------
                    // Add return message to RabbitMQ queue 
                    // -----------------------------------------------------------
                    AddMessageToQueue(exchangeName: requestRoute.TargetExchange,
                        queueName: requestRoute.TargetQueue,
                        message: jsonQueueReturnData,
                        recordCount: totalMessageCount,
                        channelPublish: channelPublish);

                    // acknowledge that we have consumed message successfully 
                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                    logger.LogInformation("Processed record in {0}ms. Total message count: {1}", stopwatch.ElapsedMilliseconds, totalMessageCount);
                }
                catch (BrokerUnreachableException ex)
                {
                    // TO DO: Add Retry logic
                    logger.LogError("Unable to communicate with RabbitMQ with Exception: {0} and Inner Exception: {1}", ex.Message, ex.InnerException.Message);
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogError("Error processing message. Error Message: {0}", ex.Message);

                    var error = new ErrorModel
                    {
                        ServiceName = workerName,
                        MessagePayload = message,
                        ErrorDescription = ex.Message,
                        Route = requestRoute
                    };

                    PublishErrorMessage(channelPublish, error);

                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                }
                finally
                {
                    stopwatch.Reset();
                }
            };
            channelConsume.BasicConsume(queue: requestRoute.SourceQueue, autoAck: false, consumer: consumer);

            Console.ReadLine();
        }

        public void NotAlertConsumeMessage(string sbsApiServer, Request requestRoute, HttpClient httpClient, IModel channelConsume, IModel channelPublish, string tmaApiServer, int apiDelayMs, string workerName)
        {
            var totalMessageCount = 0;
            string message = string.Empty;

            var consumer = new EventingBasicConsumer(channelConsume);
            consumer.Received += async (model, ea) =>
            {
                logger.LogDebug("Begin NotAlertConsumeMessage using Route: {0}", requestRoute.RouteName);

                stopwatch.Restart();

                try
                {
                    // add delay prior to calling API to ensure number of requests/second can be handled by TMA  
                    Thread.Sleep(apiDelayMs);

                    totalMessageCount++;

                    message = Encoding.UTF8.GetString(ea.Body.ToArray());

                    // -----------------------------------------------------------
                    // reformat message prior to calling API 
                    // -----------------------------------------------------------

                    // convert the json message into json object for manipulation 
                    JObject jsonObjMessage = JObject.Parse(message);

                    // identify the RequestReference from the json object; n.b. it contains hyphens in the guid 
                    string requestReferenceHyphens = (string)jsonObjMessage["RequestReference"];

                    // remove hyphens from RequestReference
                    string requestReference = requestReferenceHyphens.Replace("-", "");

                    // identify the URL from the json object 
                    string requestUrlTma = (string)jsonObjMessage["Refs"][0];

                    logger.LogDebug("NotAlertConsumeMessage extracted the Request URL: {0}", requestUrlTma);

                    Match requestUrlTmaSuffix = Regex.Match(requestUrlTma, @"(?<=[\.uk])\/api.*$");

                    if (!requestUrlTmaSuffix.Success)
                        throw new Exception($"Could not parse the Request URL: {requestUrlTma} for Request Reference: {requestReferenceHyphens}");

                    // replace the TMA base address with SBS base address 
                    string requestUri = sbsApiServer + requestUrlTmaSuffix.Value;

                    // -----------------------------------------------------------
                    // call the API 
                    // -----------------------------------------------------------

                    logger.LogInformation("NotAlertConsumeMessage calling API using Uri {0}", requestUri);

                    // call the API
                    HttpResponseMessage response = await httpClient.GetAsync(requestUri: requestUri);

                    var responseData = await GetResponseContent(response);

                    if (!response.IsSuccessStatusCode)
                        throw new Exception($"Unable to fetch data from TMA's API for Request Reference: {requestReferenceHyphens}. " +
                            $"Response Status: {response.StatusCode}. Response Content: {responseData}");

                    logger.LogDebug("NotAlertConsumeMessage API call successful. Deserialising message: {0}", responseData);

                    // -----------------------------------------------------------
                    // reformat response data   
                    // -----------------------------------------------------------

                    // format the data (to object) to remove excessive backslashes, e.g. "\"{\\\"requestReference\\\":\\\"5a1a61270f3c43d6baa47ee6a032e33e\\\"}\""
                    object formattedResponseDataObject = JsonConvert.DeserializeObject(responseData);

                    // deserialize the response into a AlertNotification object
                    // using system.text.json to ensure deciaml values retain their precision
                    AlertNotification alertNotification = System.Text.Json.JsonSerializer.Deserialize<AlertNotification>(formattedResponseDataObject.ToString());

                    alertNotification.RequestReference = requestReference;

                    // serialize the AlertNotification back into JSON 
                    string formattedResponseDataString = System.Text.Json.JsonSerializer.Serialize(alertNotification);

                                        // finally, replace any 'null' values with "" 
                    var jsonQueueReturnData = formattedResponseDataString.Replace("null", "\"\"");

                    // -----------------------------------------------------------
                    // Add return message to RabbitMQ queue 
                    // -----------------------------------------------------------
                    AddMessageToQueue(exchangeName: requestRoute.TargetExchange,
                        queueName: requestRoute.TargetQueue,
                        message: jsonQueueReturnData,
                        recordCount: totalMessageCount,
                        channelPublish: channelPublish);

                    // acknowledge that we have consumed message successfully 
                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                    logger.LogInformation("Processed record in {0}ms. Total message count: {1}", stopwatch.ElapsedMilliseconds, totalMessageCount);
                }
                catch (BrokerUnreachableException ex)
                {
                    // TO DO: Add Retry logic
                    logger.LogError("Unable to communicate with RabbitMQ with Exception: {0} and Inner Exception: {1}", ex.Message, ex.InnerException.Message);
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogError("Error processing message. Error Message: {0}", ex.Message);

                    var error = new ErrorModel
                    {
                        ServiceName = workerName,
                        MessagePayload = message,
                        ErrorDescription = ex.Message,
                        Route = requestRoute
                    };

                    PublishErrorMessage(channelPublish, error);

                    channelConsume.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                }
                finally
                {
                    stopwatch.Reset();
                }
            };
            channelConsume.BasicConsume(queue: requestRoute.SourceQueue, autoAck: false, consumer: consumer);

            Console.ReadLine();
        }

        #region Helper Methods

        private async Task<string> GetResponseContent(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();

            if (content.Contains("<html>"))
            {
                // get string within <title> tag of HTML error page
                var title = Regex.Match(content, @"(?=<title>)[\s\S]+(?<=<\/title>)");

                if (!title.Success)
                    return string.Empty;

                content = title.Value.Replace("<title>", "").Replace("</title>", "");
            }

            return content;
        }

        public void AddMessageToQueue(string exchangeName, string queueName, string message, int recordCount, IModel channelPublish)
        {
            logger.LogDebug("Attempting to publish message to {1} in {2} exchange", queueName, exchangeName);

            channelPublish.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Direct, durable: true);
            channelPublish.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            channelPublish.QueueBind(queueName, exchangeName, queueName, null);

            var body = Encoding.UTF8.GetBytes(message);

            IBasicProperties props = channelPublish.CreateBasicProperties();
            props.ContentType = "text/plain";
            props.DeliveryMode = 2;
            props.Persistent = true;

            channelPublish.BasicPublish(exchange: exchangeName, routingKey: queueName, basicProperties: props, body: body);

            if (recordCount != 0)
                logger.LogInformation("Successfully published message to Queue: {0} in Exchange: {1}. Record count: {2}", queueName, exchangeName, recordCount);
            else
                logger.LogInformation("Successfully published message to Error Queue: {0} in Exchange: {1}", queueName, exchangeName);

            logger.LogDebug("Application published message: {0}", message);
        }

        private void PublishErrorMessage(IModel channelPublish, ErrorModel error)
        {
            logger.LogDebug("Attempting to publish failed message to an Error queue. Message: {0}", error);

            var message = JsonConvert.SerializeObject(error);

            string queueName = ConstructErrorQueueName(error.Route);

            try
            {
                AddMessageToQueue(exchangeName: error.Route.TargetExchange,
                queueName: queueName,
                message: message,
                recordCount: 0,
                channelPublish: channelPublish);
            }
            catch (Exception ex)
            {
                logger.LogError("Error routing message to Error queue. Error Message: {0}", ex.Message);
                logger.LogDebug("Message that failed to queue: {0}", message);
                throw;
            }

            // Temporarily write to a file in addition to queueing the message
            // TO DO: file location/name based on configuration of individual components 
            WriteToDefaultFile(error);
        }

        private void WriteToDefaultFile(ErrorModel error)
        {
            var file = "C:\\ErrorFiles\\SMETS2RequestService_errors.txt";
            var contents = JsonConvert.SerializeObject(error);

            logger.LogDebug("Attempting to publish failed message to Error file: {0}. Message: {1}", file, contents);

            var sb = new StringBuilder();

            sb.Append(DateTime.UtcNow.ToString());
            sb.Append(": ");
            sb.Append(contents);

            var toWrite = new List<string>
            {
                sb.ToString()
            };

            File.AppendAllLines(file, toWrite);

            logger.LogDebug("Error file {0} updated with data: {1}", file, contents);
        }

        private string ConstructErrorQueueName(Request requestRoute)
        {
            var sb = new StringBuilder();

            sb.Append("req.");
            sb.Append(requestRoute.Tenant.ToLower().Substring(0, 3) + ".");

            if (requestRoute.RouteName.ToLower().Contains("fup"))
                sb.Append("fup.");
            else
                sb.Append("out.");

            sb.Append("error.q");

            return sb.ToString();
        }

        #endregion
    }
}
