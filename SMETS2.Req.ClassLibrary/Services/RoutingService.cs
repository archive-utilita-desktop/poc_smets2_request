﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SMETS2.Req.ClassLibrary.Models;

namespace SMETS2.Req.ClassLibrary.Services
{
    public class RoutingService
    {
        public static async Task<List<Request>> GetRoutes(string requestRoutingApiUrl)
        {
            using var httpClient = new HttpClient();

            var result = await httpClient.GetStringAsync(requestRoutingApiUrl);

            List<Request> requests = JsonConvert.DeserializeObject<List<Request>>(result);

            return requests;
        }
    }
}
