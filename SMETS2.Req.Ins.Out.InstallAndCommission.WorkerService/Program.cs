using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Serilog;
using Serilog.Events;
using Microsoft.AspNetCore.Hosting;
using SMETS2.Req.ClassLibrary.Contracts;
using SMETS2.Req.ClassLibrary.Repository;
using SMETS2.Req.ClassLibrary.Services;

namespace SMETS2.Req.Ins.Out.InstallAndCommission.WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string workerServiceName = "Request Installer Out Install And Commission Worker Service";

            // Setup configuration files - required for Serilog logging           
            var config = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                //.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "Production"}.json",
                    optional: true)
                .AddEnvironmentVariables()
                .Build();

            // Setup values for Serilog logging  
            string serilogFilePath = config.GetSection("SerilogFilePath").Value;
            string serilogFileNamePrefix = DateTime.Now.ToString("yyyyMMdd");
            string serilogFileNameSuffix = config.GetSection("SerilogFileNameSuffix").Value;
            string serilogFullFileName = serilogFilePath + serilogFileNamePrefix + serilogFileNameSuffix;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
                .Enrich.FromLogContext()
                .WriteTo.File(serilogFullFileName)
                .CreateLogger();

            try
            {
                Log.Information($"Starting {workerServiceName}");
                CreateHostBuilder(args).Build().Run();
                return;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, $"There was a problem starting {workerServiceName}");
                return;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    IHostEnvironment env = hostContext.HostingEnvironment;

                    MongoClient client = new MongoClient(configuration.GetConnectionString("MongoDb"));
                    IMongoDatabase database = client.GetDatabase("SMETS2RequestTracking" + env.EnvironmentName);

                    services.AddSingleton<IRequestTrackingRepository>(s => new RequestTrackingRepository(database));

                    services.AddTransient<IRequestService, RequestService>();

                    services.AddHostedService<ReqInsOutCommissionWorker>();
                    services.AddHostedService<ReqInsOutJoinWorker>();
                    services.AddHostedService<ReqInsOutCommsHubStatusUpdateWorker>();
                })
                .UseSerilog();
    }
}
