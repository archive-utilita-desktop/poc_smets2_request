using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using SMETS2.Req.ClassLibrary.Models;
using SMETS2.Req.ClassLibrary.Repository;
using SMETS2.Req.ClassLibrary.Services;
using Microsoft.Extensions.DependencyInjection;
using SMETS2.Req.ClassLibrary.Contracts;

namespace SMETS2.Req.Ins.Out.InstallAndCommission.WorkerService
{
    public class ReqInsOutCommsHubStatusUpdateWorker : BackgroundService
    {
        private readonly string workerName = nameof(ReqInsOutCommsHubStatusUpdateWorker);
        private readonly string routeName;
        private readonly int apiDelayMs;

        //RabbitMQ variables
        private ConnectionFactory factory = new ConnectionFactory();
        private IConnection connection;
        private IModel channelConsume;
        private IModel channelPublish;

        //HttpClient setup 
        private HttpClient httpClient;

        private readonly ILogger<ReqInsOutCommsHubStatusUpdateWorker> logger;
        private readonly IConfiguration configuration;
        private readonly IRequestTrackingRepository requestTrackingRepository;
        private readonly IRequestService requestService;

        public ReqInsOutCommsHubStatusUpdateWorker(ILogger<ReqInsOutCommsHubStatusUpdateWorker> logger, IConfiguration configuration, IRequestTrackingRepository requestTrackingRepository, IRequestService requestService)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.requestTrackingRepository = requestTrackingRepository;
            this.requestService = requestService;

            routeName = configuration.GetSection("ReqInsOutRouteNameCommsHubStatusUpdate").Value;
            apiDelayMs = int.TryParse(configuration.GetSection("ApiDelayMs").Value, out apiDelayMs) ? apiDelayMs : 300;
            configuration.GetSection("RabbitMqConnection").Bind(factory);
            httpClient = new HttpClient();
        }

        public override Task StartAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation($"*** {workerName} starting ***");

            connection = factory.CreateConnection();
            channelConsume = connection.CreateModel();
            channelConsume.BasicQos(0, 1, false);
            channelPublish = connection.CreateModel();

            return base.StartAsync(stoppingToken);
        }

        public override Task StopAsync(CancellationToken stoppingToken)
        {
            channelConsume.Close();
            channelPublish.Close();
            connection.Close();

            logger.LogInformation($"*** {workerName} stopping ***");
            return base.StopAsync(stoppingToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //--------------------------------------------------
            // get routing information for worker service 
            //--------------------------------------------------
            var requestRoutingApiUrl = configuration.GetSection("RequestRoutingApiUrl").Value;
            var requests = await RoutingService.GetRoutes(requestRoutingApiUrl);
            Request requestRoute = requests.FirstOrDefault(s => s.RouteName == routeName);

            var sbsApiServer = requestRoute.Tenant == "Supplier"
                ? configuration.GetSection("SbsApiServerSupplier").Value
                : configuration.GetSection("SbsApiServerInstaller").Value;

            //--------------------------------------------------
            // perform RabbitMQ consumer processing
            //--------------------------------------------------
            channelConsume.QueueDeclare(queue: requestRoute.SourceQueue, durable: true, exclusive: false, autoDelete: false, arguments: null);

            try
            {
                requestService.DccConsumeMessage(sbsApiServer, requestRoute, httpClient, channelConsume, channelPublish, requestTrackingRepository, apiDelayMs, workerName);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                logger.LogError(" Application will be terminated ... ");
                await StopAsync(new CancellationToken(true));
            }
        }
    }
}
