﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SMETS2.Request.Web.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;

namespace SMETS2.Request.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;
        private readonly IWebHostEnvironment env;

        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment env)
        {
            this.logger = logger;
            this.env = env;
        }

        public IActionResult Index()
        {
            ViewBag.environmentName = env.EnvironmentName;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
