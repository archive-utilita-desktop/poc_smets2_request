﻿using Microsoft.AspNetCore.Mvc;
using SMETS2.Req.ClassLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using SMETS2.Req.ClassLibrary.Models;

namespace SMETS2.Request.Web.Controllers
{
    public class RequestTrackingController : Controller
    {
        private readonly IRequestTrackingRepository requestTrackingRepository;
        private readonly IWebHostEnvironment env;

        public RequestTrackingController(IRequestTrackingRepository requestTrackingRepository, IWebHostEnvironment env)
        {
            this.requestTrackingRepository = requestTrackingRepository;
            this.env = env;
        }

        // GET: RequestTracking
        public IActionResult Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            ViewBag.environmentName = env.EnvironmentName;

            ViewData["CurrentSort"] = sortOrder;
            ViewData["RequestReferenceDateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "date_asc" : "";
            ViewData["RequestReferenceSortParm"] = sortOrder == "ref" ? "ref_desc" : "ref";
            ViewData["RequestGuidSortParm"] = sortOrder == "guid" ? "guid_desc" : "guid";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            IEnumerable<RequestTracking> requestTrackings = requestTrackingRepository.GetAll();

            //IEnumerable<RequestTracking> requestTrackings =
            //    requestTrackings0.OrderByDescending(r => r.RequestReferenceDate).Take(10000); 


            if (!String.IsNullOrEmpty(searchString))
            {
                requestTrackings = requestTrackings.Where(s => s.RequestReference == searchString
                                                               || s.RequestGuid == searchString);
            }

            switch (sortOrder)
            {
                case "date_asc":
                    requestTrackings = requestTrackings.OrderBy(s => s.RequestReferenceDate);
                    break;
                case "ref":
                    requestTrackings = requestTrackings.OrderBy(s => s.RequestReference);
                    break;
                case "ref_desc":
                    requestTrackings = requestTrackings.OrderByDescending(s => s.RequestReference);
                    break;
                case "guid":
                    requestTrackings = requestTrackings.OrderBy(s => s.RequestGuid);
                    break;
                case "guid_desc":
                    requestTrackings = requestTrackings.OrderByDescending(s => s.RequestGuid);
                    break;
                default:
                    requestTrackings = requestTrackings.OrderByDescending(s => s.RequestReferenceDate);
                    break;
            }

            int pageSize = 10;
            return View(PaginatedList<RequestTracking>.Create(requestTrackings.AsQueryable(), pageNumber ?? 1, pageSize));

            //return View(requestTrackings.ToList());
        }

        // GET: RequestTracking/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var requestTracking = requestTrackingRepository.Get(id);
            if (requestTracking == null)
            {
                return NotFound();
            }
            return View(requestTracking);
        }

        // GET: RequestTracking/Create
        [NonAction]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RequestTracking/Create/5
        [NonAction]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RequestTracking requestTracking)
        {
            if (ModelState.IsValid)
            {
                requestTrackingRepository.Create(requestTracking);
                return RedirectToAction(nameof(Index));
            }
            return View(requestTracking);
        }

        // GET: RequestTracking/Edit/5
        [NonAction]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var requestTracking = requestTrackingRepository.Get(id);
            if (requestTracking == null)
            {
                return NotFound();
            }
            return View(requestTracking);
        }

        // POST: RequestTracking/Edit/5
        [NonAction]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, RequestTracking requestTracking)
        {
            if (id != requestTracking.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                requestTrackingRepository.Update(id, requestTracking);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(requestTracking);
            }
        }

        // GET: RequestTracking/Delete/5
        [NonAction]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var requestTracking = requestTrackingRepository.Get(id);
            if (requestTracking == null)
            {
                return NotFound();
            }
            return View(requestTracking);
        }

        // POST: RequestTracking/Delete/5
        [NonAction]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                var requestTracking = requestTrackingRepository.Get(id);

                if (requestTracking == null)
                {
                    return NotFound();
                }

                requestTrackingRepository.Delete(requestTracking.Id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
